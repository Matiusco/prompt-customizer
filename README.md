# Prompt Customizer

- Este projeto tem uma proposta colaborativa para integrar vários modelos de prompts usados no seu terminal bash de usuário. 
 
- O foco central será escolher entre as grades de cores os seus respectivos visuais e divisores para uso em 1 ou duas linhas de prompt. 
 
- Você também estará livre para propor facilidades de operações para seu terminal. Para isso chamaremos de plugins. 
- Você poderá criar seus próprios plugins modelos e incluir como desejar. 
 
- A princípio este projeto visa atender ao padrão .bash usuário distribuído na versão Debian Stable 11.
  
